class BettingGame < GameType
  validates :classification, presence: true, inclusion: { in: ['Local', 'Regional', 'National'],
    message: "%{value} is not a valid classification " }

  validates :legality, presence: true, inclusion: { in: %w(Legal Depends Unclear),
    message: "%{value} is not a valid legality " }
end
