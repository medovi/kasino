class CardGame < GameType
  validates :variant_name, presence: true, inclusion: { in: ['Beginners', 'Really unfair', 'Standard'],
    message: "%{value} is not a valid variant name" }
end

# There exist different types of games within the same table, each with different classes to model their behavior.
# So, BlackJack is a CardGame, HorseRace is a BetterGame, etc.
