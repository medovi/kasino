class CreateGameTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :game_types do |t|
      t.string :type
      t.string :variant_name
      t.integer :deck_size
      t.boolean :rigged
      t.string :classification
      t.string :legality
      t.integer :max_betters
      t.string :project_gain
      t.references :casino
      t.timestamps
    end
  end
end
