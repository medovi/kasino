class CreateCasinos < ActiveRecord::Migration[5.1]
  def change
    create_table :casinos do |t|
      t.string :name
      t.string :full_address

      t.timestamps
    end
  end
end
