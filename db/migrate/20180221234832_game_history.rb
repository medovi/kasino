class GameHistory < ActiveRecord::Migration[5.1]
  def change
    create_table :game_histories do |t|
     t.belongs_to :casino, index: true
     t.belongs_to :game_type_id, index: true
     t.datetime :date_acquired,
     t.float :income_generated
     t.timestamps
   end
  end
end
